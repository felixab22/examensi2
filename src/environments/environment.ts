// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDQbBfbLMQ4zpf_g7uJP35M1gPvJzFTw74",
    authDomain: "documentossistemas-938aa.firebaseapp.com",
    databaseURL: "https://documentossistemas-938aa.firebaseio.com",
    projectId: "documentossistemas-938aa",
    storageBucket: "",
    messagingSenderId: "677684255860",
    appId: "1:677684255860:web:7f930de89afc2c16"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
