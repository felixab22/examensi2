export class ProfesorModel {
    $key?: string;
    estudio: string;
    nombre: string;
    apellidos: string;
}