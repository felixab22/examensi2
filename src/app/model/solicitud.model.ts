export class BachillerModel {
    nombre: string;
    dni: string;
    codeUnsch: string;
    domicilio: string;
    escuela: string;
    facultad: string;
    fecha: any;
}
export class UnicoModel {
    nombre: string;
    dni: string;
    codeUnsch: string;
    domicilio: string;
    distrito: string;
    provincia: string;
    fecha: any;
    asignatura: string;
    sigla: string;
    credito: string;
    bolera?: string;

}
export class PraticasModel {
    nombre: string;
    dni: string;
    codeUnsch: string;
    domicilio: string;
    escuela: string;
    facultad: string;
    organizacion: string;
    representName: string;
    cargo: string;
    area: string;
    fecha: any;
    fechaInicio: any;
    fechaFin: any;
}