export class AlumnoModel {
    public $key?: string;
    public serie: string;
    public alumno: string;
    public creditos: string;
    public direccion: string;
    public dni: string;
    public codigo: string;
    public escuela: string;
    public facultad: string;
    public semestreInicio?: string;
    public semestreFin?: string;
    public reciboInicio?: string;
    public reciboFin?: string;
    public fechaInicio?: string;
    public fechaFin?: string;
    public constanciaInicio?: string;
    public constanciaFin?: string;
}
