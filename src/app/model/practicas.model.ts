
export class PracticaModel {
    $key: string;
    tituloPracticas: string;
    anio: number;
    semestre: string;
    alumno: string;
    fecha: string;
    notaInforme: number;
    notaExposicion: number;
    notaPreguntas: number;
    notaPromedio: number;
    juradoUno: string;
    juradoDos: string;
    juradoTres: string;
}