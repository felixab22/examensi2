export class EgresadoModel {
    public idproveedor: number;
    public nombreEgresado: string;
    public escuela: string;
    public facultad: string;
    public codeUnsch: string;
    public dni: string;
    public semestre: string;
    public dateSemestre: any;
    public codeRecibo: string;
    public dateConstancia: string;
}
