import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { ProfesorModel } from '../model/profesor.model';


@Injectable({
    providedIn: 'root'
})
export class ProfesorService {
    Lista: AngularFireList<any>;
    profesores: ProfesorModel[];
    constructor(
        private http: HttpClient,
        public _firebase: AngularFireDatabase
    ) {
        this.profesores = new Array<ProfesorModel>();
    }
    getAllProfesores() {
        return this.Lista = this._firebase.list('profesor');
    }
    SaveProfesor(profesor: ProfesorModel) {
        this.Lista.push({
            nombre: profesor.nombre,
            estudio: profesor.estudio,
            apellidos: profesor.apellidos,
        });
    }
    UpdateProfesor(profesor: ProfesorModel) {
        this.Lista.update(profesor.$key, {
            nombre: profesor.nombre,
            estudio: profesor.estudio,
            apellidos: profesor.apellidos,
        });
    }
    deleteProfesor($key: string) {
        this.Lista.remove($key);
    }
    listarProfesores(): ProfesorModel[] {
        this.profesores = new Array<ProfesorModel>();
        this.getAllProfesores().snapshotChanges().forEach(res => {
            res.forEach(alumnosres => {
                let user = alumnosres.payload.toJSON();
                // if(user.)
                this.profesores.push(user as ProfesorModel);
            })
        });
        return this.profesores;
    }


}