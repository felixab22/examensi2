import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { PracticaModel } from '../model/practicas.model';


@Injectable({
    providedIn: 'root'
})
export class PracticasService {
    Lista: AngularFireList<any>;
    constructor(
        private http: HttpClient,
        public _firebase: AngularFireDatabase
    ) {

    }
    getAllPracticas() {
        return this.Lista = this._firebase.list('practica');
    }
    SaveOrUpdate(practica: PracticaModel) {
        this.Lista.push({
            tituloPracticas: practica.tituloPracticas,
            anio: practica.anio,
            semestre: practica.semestre,
            alumno: practica.alumno,
            fecha: practica.fecha,
            notaInforme: practica.notaInforme,
            notaExposicion: practica.notaExposicion,
            notaPreguntas: practica.notaPreguntas,
            notaPromedio: practica.notaPromedio,
            juradoUno: practica.juradoUno,
            juradoDos: practica.juradoDos,
            juradoTres: practica.juradoTres,
        })
    }


}