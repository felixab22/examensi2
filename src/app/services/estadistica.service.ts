import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { ProfesorModel } from '../model/profesor.model';
// import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class EstadisticaService {
    salida: EstadisticaModel;
    public menus: any[] = [
        {
            id: 1,
            tipo: 'Constancia de ingreso'
        },
        {
            id: 2,
            tipo: 'Constancia de egreso'
        },
        {
            id: 3,
            tipo: 'Constancia de serie'
        }
    ];


    Lista: AngularFireList<any>;
    estadistica: any[];
    constructor(
        public _firebase: AngularFireDatabase,
        // public fire: AngularFirestore
    ) {
        this.estadistica = new Array<any>();
        this.salida = new EstadisticaModel();
    }
    getAllProfesores() {
        return this.Lista = this._firebase.list('estadistica');
    }
    SaveOrUpdate(practica: any) {
        this.Lista.push({
            entrada: practica
        })
    }

    getgraficoEgresado(): EstadisticaModel {
        this.salida.cantidad = [300, 50, 100];
        this.salida.tipo = ['2017', '2018', '2019'];
        return this.salida;

    }
    getgraficoIngreso(): EstadisticaModel {
        this.salida.cantidad = [100, 120, 110];
        this.salida.tipo = ['2017', '2018', '2019'];
        return this.salida;
    }

    listarProfesores(): ProfesorModel[] {
        let retorno = new Array<any>();
        this.getAllProfesores().snapshotChanges().forEach(res => {
            res.forEach(alumnosres => {
                let user = alumnosres.payload.toJSON();
                // if(user.)
                retorno.push(user as ProfesorModel);
            })
        });
        return retorno;
    }


}
export class EstadisticaModel {
    tipo: any;
    cantidad: any;
}