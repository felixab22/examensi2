import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { AlumnoModel } from '../model/alumno.model';


@Injectable({
    providedIn: 'root'
})
export class AlumnosService {
    ListaAlumnos: AngularFireList<any>;
    listaGuardado: AngularFireList<any>;
    alumnosSima: Array<AlumnoModel>;
    alumnosSistemas: Array<AlumnoModel>;
    encontrado: AlumnoModel;
    encontrado2: AlumnoModel;
    salida: AlumnoModel;
    constructor(
        private http: HttpClient,
        public _firebase: AngularFireDatabase
    ) {
        this.ListaAlumnos = _firebase.list('alumnos')
        this.listaGuardado = _firebase.list('sistemas')
        this.alumnosSistemas = new Array<AlumnoModel>();
        this.alumnosSima = new Array<AlumnoModel>();
        this.salida = new AlumnoModel();
        this.encontrado = new AlumnoModel();
        this.encontrado2 = new AlumnoModel();
    }
    getAlumnosSima() {
        return this.ListaAlumnos;
    }
    getAlumnosSistemas() {
        return this.listaGuardado;
    }
    listarAlumnosSima(): AlumnoModel[] {
        this.getAlumnosSima().snapshotChanges().forEach(res => {
            res.forEach(alumnosres => {
                let user = alumnosres.payload.toJSON();
                this.alumnosSima.push(user as AlumnoModel);
            })
        });
        return this.alumnosSima;
    }
    listarAlumnosSistemas(): AlumnoModel[] {
        this.getAlumnosSistemas().snapshotChanges().forEach(res => {
            res.forEach(alumnosres => {
                let user = alumnosres.payload.toJSON();
                this.alumnosSistemas.push(user as AlumnoModel);
            })
        });
        return this.alumnosSistemas;
    }

    buscarAllSistemas(codigo: string): AlumnoModel {
        this.salida = this.buscarAlumnnoSistemas(codigo);
        if (this.salida === null) {
            this.salida = this.buscarAlumnnoSima(codigo);
        }
        return this.salida;
        // if (this.buscarAlumnnoSistemas(codigo) === null) {
        //     // if (this.buscarAlumnnoSima(codigo) === null) {
        //     //     // return this.salida = null;
        //     //     // return this.salida = null;
        //     // } else {
        //     // }
        // } else {

        // }
    }
    SaveAlumnoSistemas(alumno: AlumnoModel) {
        this.listaGuardado.push({
            alumno: alumno.alumno,
            codigo: alumno.codigo,
            creditos: alumno.creditos,
            dni: alumno.dni,
            direccion: alumno.direccion,
            escuela: alumno.escuela,
            facultad: alumno.facultad,
            semestreInicio: alumno.semestreInicio,
            semestreFin: alumno.semestreFin,
            reciboInicio: alumno.reciboInicio,
            reciboFin: alumno.reciboFin,
            fechaInicio: alumno.fechaInicio,
            fechaFin: alumno.fechaFin,
            constanciaInicio: alumno.constanciaInicio,
            constanciaFin: alumno.constanciaFin,
        });
    }
    UpdateAlumnoSistemas(alumno: AlumnoModel) {
        this.listaGuardado.update(alumno.codigo, {
            alumno: alumno.alumno,
            codigo: alumno.codigo,
            creditos: alumno.creditos,
            dni: alumno.dni,
            direccion: alumno.direccion,
            escuela: alumno.escuela,
            facultad: alumno.facultad,
            semestreInicio: alumno.semestreInicio,
            semestreFin: alumno.semestreFin,
            reciboInicio: alumno.reciboInicio,
            reciboFin: alumno.reciboFin,
            fechaInicio: alumno.fechaInicio,
            fechaFin: alumno.fechaFin,
            constanciaInicio: alumno.constanciaInicio,
            constanciaFin: alumno.constanciaFin,
        });
    }
    buscarAlumnnoSistemas(codigo: string): AlumnoModel {
        for (let item of this.alumnosSistemas) {
            if (item.codigo === codigo) {
                this.encontrado2 = item;
                return this.encontrado2;
            } else {
                this.encontrado2 = null;
            }
        }
        console.log('paso Sistemas', this.encontrado2);
        return this.encontrado2;
    }
    buscarAlumnnoSima(codigo: string): AlumnoModel {
        console.log('paso sima');

        for (let item of this.alumnosSima) {
            if (item.codigo === codigo) {
                this.encontrado = item;
                return this.encontrado;
            } else {
                this.encontrado = null;
            }
        }
        console.log('paso 4');
        return this.encontrado;
    }


}
