import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  AlumnosService,
  EstadisticaService,
  ProfesorService
} from './services.index';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    AlumnosService,
    EstadisticaService,
    ProfesorService
  ]
})
export class ServicesModule { }
