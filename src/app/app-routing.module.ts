import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages/pages.component';
import { BienvenidoComponent } from './pages/bienvenido/bienvenido.component';


const routes: Routes = [
  { path: '', component: BienvenidoComponent },
  {
    path: 'Sistemas-unsch',
    component: PagesComponent,
    children:
      [
        {
          path: 'Nosotros',
          loadChildren: './pages/nosotros/nosotros.module#NosotrosModule'
        },
        {
          path: 'Constancia',
          loadChildren: './pages/constancia/constancia.module#ConstanciaModule'
        },
        {
          path: 'Solicitud',
          loadChildren: './pages/solicitud/solicitud.module#SolicitudModule'
        },
        {
          path: 'Config',
          loadChildren: './pages/config/config.module#ConfigModule'
        },
        {
          path: 'Practicas',
          loadChildren: './pages/practicas/practicas.module#PracticasModule'
        }

      ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
