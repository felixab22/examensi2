import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MesesPipe } from './mes.pipe';

@NgModule({
  declarations: [
    MesesPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [MesesPipe]
})
export class PipesModule { }
