import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GradoBachillerComponent } from './grado-bachiller/grado-bachiller.component';
import { PracticasComponent } from './practicas/practicas.component';
import { EgresadoComponent } from './egresado/egresado.component';
import { UnicoComponent } from './unico/unico.component';

const routes: Routes = [
  {
    path: 'Bachiller',
    component: GradoBachillerComponent
  },
  {
    path: 'Practicas',
    component: PracticasComponent
  },
  {
    path: 'Egresado',
    component: EgresadoComponent
  },
  {
    path: 'Unico',
    component: UnicoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SolicitudRoutingModule { }
