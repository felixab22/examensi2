import { Component, OnInit } from '@angular/core';
import { DateModel } from 'src/app/model/date.model';
import { BachillerModel } from '../../../model/solicitud.model';
import { AlumnoModel } from 'src/app/model/alumno.model';
import { AlumnosService } from 'src/app/services/services.index';
declare var swal: any;

@Component({
  selector: 'app-grado-bachiller',
  templateUrl: './grado-bachiller.component.html',
  styleUrls: ['./grado-bachiller.component.scss']
})
export class GradoBachillerComponent implements OnInit {
  newDate: DateModel;
  newBachiller: BachillerModel;
  anioName = 'Nombre del año';
  encontrado: AlumnoModel;
  alumnos: AlumnoModel[];
  buscaCodigo: string;
  requisitos: { id: number; tipo: string; }[];
  constructor(
    private _AlumnoSrv: AlumnosService
  ) {
    this.encontrado = new AlumnoModel();
    this.newBachiller = new BachillerModel();
  }

  ngOnInit() {
    this.alumnos = this._AlumnoSrv.listarAlumnosSima();
    if (localStorage.getItem('data') !== null) {
      this.newDate = JSON.parse(localStorage.getItem('data'));
      this.anioName = this.newDate.anio;
    } else {
      this.anioName = 'Nombre del año'
    }
    this.requisitos = [
      { id: 1, tipo: 'Certificado de estudios originales' },
      { id: 2, tipo: 'Copia legalizada de DNI' },
      { id: 2, tipo: 'Declaración jurada de no tener antecedentes jidiciales y penales' },
      { id: 2, tipo: 'Fotografia en CD con rotula en un sobre manila' },
      { id: 2, tipo: '04 fotografias tamaño pasaporte' },
      { id: 2, tipo: 'Certificado de Práticas Pre-profesionales' },
      { id: 2, tipo: 'Constancia de ingreso, no adeudar a la biblioteca, no adeudar al comedor y residencia de estudiantes y no adeudar a la facultad' },
      { id: 2, tipo: 'Recibo de tesoreria' },
    ];
  }
  bucarxCodigo(basicModal1) {
    this.encontrado = this._AlumnoSrv.buscarAllSistemas(this.buscaCodigo);
    if (this.encontrado !== null) {
      this.newBachiller.dni = this.encontrado.dni;
      this.newBachiller.codeUnsch = this.encontrado.codigo;
      this.newBachiller.escuela = this.encontrado.escuela;
      this.newBachiller.domicilio = this.encontrado.direccion;
      this.newBachiller.facultad = this.encontrado.facultad;
      this.newBachiller.nombre = this.encontrado.alumno;
    } else {
      basicModal1.hide();
      this.newBachiller = new BachillerModel();
      swal('Mal!', 'Alumno no encontrado', 'warning');
    }

  }


  SaveOrUpdateBachiller(basicModal1) {
    basicModal1.hide();
    console.log(this.newBachiller);

  }




  createPDF() {
    var sTable = document.getElementById('imprimirGradoBachiller').innerHTML;
    var style = "<style>";
    style = style + "div.a4page {font-family: Times New Roman; width: 210mm; height: 245mm; padding: 100px;}";
    style = style + "p { text-align: justify;text-justify: inter-word;font-size: 15px;}";
    style = style + "strong  {font-weight: 700;}";
    style = style + "p.solicito { margin: 20px 20px 20px 0px;text-align: right;float: right;}";
    style = style + "h4 {margin-top: 10px;margin-bottom: 10px; font-weight: 500;color: black;}";
    style = style + ".cabecera {display: grid; grid-template-columns: 50% 50%;}";
    style = style + ".center {text-align: center;}";
    style = style + ".abajo {padding: 0;margin: 140px 0 0 0;}";

    style = style + "</style>";
    // CREATE A WINDOW OBJECT.
    var win = window.open('', '', 'height=700,width=700');
    win.document.write('<html><head>');
    win.document.write('<title>Solicitud de grado academico</title>');   // <title> FOR PDF HEADER.
    win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write('</body></html>');
    win.document.close(); 	// CLOSE THE CURRENT WINDOW.
    win.print();    // PRINT THE CONTENTS.
  }
}
