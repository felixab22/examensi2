import { Component, OnInit } from '@angular/core';
import { DateModel } from 'src/app/model/date.model';
import { UnicoModel } from 'src/app/model/solicitud.model';

@Component({
  selector: 'app-unico',
  templateUrl: './unico.component.html',
  styleUrls: ['./unico.component.scss']
})
export class UnicoComponent implements OnInit {

  newDate: DateModel;
  newUnico: UnicoModel;
  anioName = 'Nombre del año'
  constructor() {
    this.newUnico = new UnicoModel();
  }


  ngOnInit() {
    if (localStorage.getItem('data') !== null) {
      this.newDate = JSON.parse(localStorage.getItem('data'));
      this.anioName = this.newDate.anio;
    } else {
      this.anioName = 'Nombre del año'
    }
  }
  SaveOrUpdatePracticas(basicModal5) {
    basicModal5.hide();
  }

  createPDF() {
    var sTable = document.getElementById('imprimirGradoBachiller').innerHTML;
    var style = "<style>";
    style = style + "div.a4page {font-family: Times New Roman; width: 210mm; height: 245mm; padding: 100px;}";
    style = style + "p { text-align: justify;text-justify: inter-word;font-size: 15px;}";
    style = style + "strong  {font-weight: 700;}";
    style = style + "p.solicito { margin: 20px 20px 20px 0px;text-align: right;float: right;}";
    style = style + "h4 {margin-top: 10px;margin-bottom: 10px; font-weight: 500;color: black;}";
    style = style + ".cabecera {display: grid; grid-template-columns: 50% 50%;}";
    style = style + "table {width: 100%;border-collapse:collapse}";
    style = style + "tr {border: 1px solid black;text-align: center;}";
    style = style + "th {border: 1px solid black;font-weight: 700; background-color: #d0d6e2;}";
    style = style + "td {border: 1px solid black;}";
    style = style + ".center {text-align: center;}";
    style = style + ".abajo {padding: 0;margin: 140px 0 0 0;}";

    style = style + "</style>";
    // CREATE A WINDOW OBJECT.
    var win = window.open('', '', 'height=700,width=700');
    win.document.write('<html><head>');
    win.document.write('<title>Solicitud curso unico</title>');   // <title> FOR PDF HEADER.
    win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write('</body></html>');
    win.document.close(); 	// CLOSE THE CURRENT WINDOW.
    win.print();    // PRINT THE CONTENTS.
  }

}
