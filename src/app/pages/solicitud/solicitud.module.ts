import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SolicitudRoutingModule } from './solicitud-routing.module';
import { GradoBachillerComponent } from './grado-bachiller/grado-bachiller.component';

import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { ServicesModule } from 'src/app/services/services.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { PipesModule } from 'src/app/pipes/pipes.module';

import { PracticasComponent } from './practicas/practicas.component';
import { EgresadoComponent } from './egresado/egresado.component';
import { UnicoComponent } from './unico/unico.component';


@NgModule({
  declarations: [
    GradoBachillerComponent,
    PracticasComponent,
    EgresadoComponent,
    UnicoComponent,
  ],
  imports: [
    CommonModule,
    SolicitudRoutingModule,
    //desde aqui para MDBootstrap
    MDBBootstrapModule.forRoot(),
    ServicesModule,
    Ng2SearchPipeModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    PipesModule
    //siempre importar
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class SolicitudModule { }
