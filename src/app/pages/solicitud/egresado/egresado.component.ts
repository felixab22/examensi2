import { Component, OnInit } from '@angular/core';
import { DateModel } from 'src/app/model/date.model';
import { BachillerModel } from 'src/app/model/solicitud.model';
import { AlumnoModel } from 'src/app/model/alumno.model';
import { AlumnosService } from 'src/app/services/services.index';
declare var swal: any;

@Component({
  selector: 'app-egresado',
  templateUrl: './egresado.component.html',
  styleUrls: ['./egresado.component.scss']
})
export class EgresadoComponent implements OnInit {
  newDate: DateModel;
  newPractica: BachillerModel;
  anioName = 'Nombre del año'
  encontrado: AlumnoModel;
  alumnos: AlumnoModel[];
  buscaCodigo: string;
  constructor(
    private _AlumnoSrv: AlumnosService
  ) {
    this.encontrado = new AlumnoModel();
    this.newPractica = new BachillerModel();
  }

  ngOnInit() {
    this.alumnos = this._AlumnoSrv.listarAlumnosSima();

    if (localStorage.getItem('data') !== null) {
      this.newDate = JSON.parse(localStorage.getItem('data'));
      this.anioName = this.newDate.anio;
    } else {
      this.anioName = 'Nombre del año'
    }
  }
  bucarxCodigo(basicModal1) {
    this.encontrado = this._AlumnoSrv.buscarAllSistemas(this.buscaCodigo);
    if (this.encontrado !== null) {
      this.newPractica.dni = this.encontrado.dni;
      this.newPractica.codeUnsch = this.encontrado.codigo;
      this.newPractica.escuela = this.encontrado.escuela;
      this.newPractica.facultad = this.encontrado.facultad;
      this.newPractica.nombre = this.encontrado.alumno;
      this.newPractica.domicilio = this.encontrado.direccion;
    } else {
      basicModal1.hide();

      swal('Mal!', 'Alumno no encontrado', 'warning');
    }

  }


  SaveOrUpdateEgresado(basicModal10) {
    basicModal10.hide()
  }

  createPDF() {
    var sTable = document.getElementById('imprimirGradoBachiller').innerHTML;
    var style = "<style>";
    style = style + "div.a4page {font-family: Times New Roman; width: 210mm; height: 245mm; padding: 100px;}";
    style = style + "p { text-align: justify;text-justify: inter-word;font-size: 15px;}";
    style = style + "strong  {font-weight: 700;}";
    style = style + "p.solicito { margin: 20px 20px 20px 0px;text-align: right;float: right;}";
    style = style + "h4 {margin-top: 10px;margin-bottom: 10px; font-weight: 500;color: black;}";
    style = style + ".cabecera {display: grid; grid-template-columns: 50% 50%;}";
    style = style + ".center {text-align: center;}";
    style = style + ".abajo {padding: 0;margin: 140px 0 0 0;}";

    style = style + "</style>";
    // CREATE A WINDOW OBJECT.
    var win = window.open('', '', 'height=700,width=700');
    win.document.write('<html><head>');
    win.document.write('<title>Solicitudo constancia</title>');   // <title> FOR PDF HEADER.
    win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write('</body></html>');
    win.document.close(); 	// CLOSE THE CURRENT WINDOW.
    win.print();    // PRINT THE CONTENTS.
  }

}
