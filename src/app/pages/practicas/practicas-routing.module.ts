import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PracticasComponent } from './practica/practicas.component';

const routes: Routes = [
  {
    path: 'Practicas',
    component: PracticasComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PracticasRoutingModule { }
