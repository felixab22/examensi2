import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PracticasRoutingModule } from './practicas-routing.module';
import { PracticasComponent } from './practica/practicas.component';

import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { ServicesModule } from 'src/app/services/services.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { PipesModule } from 'src/app/pipes/pipes.module';

// firebase

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from 'src/environments/environment';


@NgModule({
  declarations: [PracticasComponent],
  imports: [
    CommonModule,
    PracticasRoutingModule,
    //desde aqui para MDBootstrap
    MDBBootstrapModule.forRoot(),
    ServicesModule,
    Ng2SearchPipeModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    PipesModule,
    //siempre importar
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule, // for database
  ]
})
export class PracticasModule { }
