import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { ConstanciaRoutingModule } from './constancia-routing.module';
import { EgresoComponent } from './egreso/egreso.component';
// import { BrowserModule } from '@angular/platform-browser';
// import { AppRoutingModule } from 'src/app/app-routing.module';
import { ServicesModule } from 'src/app/services/services.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { IngresoComponent } from './ingreso/ingreso.component';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { EstadisticaComponent } from './estadistica/estadistica.component';
import { SerieComponent } from './serie/serie.component';
import { ConfigModule } from '../config/config.module';

@NgModule({
  declarations: [
    EgresoComponent,
    IngresoComponent,
    EstadisticaComponent,
    SerieComponent
  ],
  imports: [
    ConfigModule,
    CommonModule,
    ConstanciaRoutingModule,
    //desde aqui para MDBootstrap
    MDBBootstrapModule.forRoot(),
    ServicesModule,
    Ng2SearchPipeModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    PipesModule
    //siempre importar
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class ConstanciaModule { }
