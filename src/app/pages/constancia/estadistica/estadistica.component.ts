import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-estadistica',
  templateUrl: './estadistica.component.html',
  styleUrls: ['./estadistica.component.scss']
})
export class EstadisticaComponent implements OnInit {

  @Output() cambioValor: EventEmitter<any> = new EventEmitter()

  // inicio grafica pastel 
  public chartType: string = 'bar';

  public chartDatasets: Array<any> = [
    { data: null, label: 'Graficas' }
  ];

  public chartLabels: Array<any> = null;
  public chartColors: Array<any> = [
    {
      backgroundColor: ['#F7464A', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360', '#2BBBAD', '#4285F4', '#aa66cc', '#1976d2', '#cddc39',
        '#ff8a65', '#f4511e', '#a1887f', '#3e2723', '#212121', '#dd2c00', '#795548', '#ff4081', '#6200ea', '#l80d8ff', '#1b5e20', '#59698d', '#00c853', '#aa00ff', '#1c2331'],

      hoverBackgroundColor: ['#FF5A5E', '#5AD3D1', '#FFC870', '#A8B3C5', '#616774', '#00695c', '#0d47a1', '#9933CC', '#64b5f6', '#f0f4c3',
        '#ffccbc', '#ff7043', '#d7ccc8', '#8d6e63', '#9e9e9e', '#ff6e40', '#bcaaa4', '#c51162', '#b388ff', '#0091ea', '#66bb6a', '#b1bace', '#69f0ae', '#e040fb', '#2e3951'],
      borderWidth: 2,
    }
  ];
  public chartOptions: any = {
    responsive: true
  };
  pastel: boolean;
  barras: boolean;
  tiposgraficas: { id: number; descripcion: string; tipo: string; }[];
  total: any;
  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }
  // fin grafica pastel


  constructor(
    // private _graficasSrv: GraficasService
  ) {

  }

  ngOnInit() {
    this.tiposgraficas = [
      { id: 1, descripcion: 'Pastel', tipo: 'pie' },
      { id: 2, descripcion: 'Dona', tipo: 'doughnut' },
      { id: 3, descripcion: 'Barras', tipo: 'bar' },
      { id: 4, descripcion: 'Barras horizontal', tipo: 'horizontalBar' },
      { id: 5, descripcion: 'Polar', tipo: 'polarArea' },
      { id: 6, descripcion: 'Line', tipo: 'line' },
      { id: 7, descripcion: 'Radar', tipo: 'radar' }

    ]
  }

  setNewGraficas(tipo: any): void {
    // console.log(tipo);
    this.chartType = tipo;
  }

  graficas(event: any) {
    console.log(event);

    // camnbio grafica pastel
    this.chartDatasets[0].data = event.cantidad;
    this.total = event.cantidad.reduce(function (a, b) { return a + b; }, 0);
    this.chartLabels = event.tipo;
    // fin


  }


}
