import { Component, OnInit } from '@angular/core';
import { AlumnoModel } from 'src/app/model/alumno.model';
import { AlumnosService } from 'src/app/services/services.index';
declare var swal: any;

@Component({
  selector: 'app-serie',
  templateUrl: './serie.component.html',
  styleUrls: ['./serie.component.scss']
})
export class SerieComponent implements OnInit {

  editar = false;
  encontrado: AlumnoModel;
  buscaCodigo: string;
  mesEgreso: string;
  mesconstancia: string;
  day = new Date().toISOString().substr(8, 2);
  month = new Date().toISOString().substr(5, 2);
  year = new Date().toISOString().substr(0, 4);
  constructor(
    private _AlumnoSrv: AlumnosService
  ) {
    this.encontrado = new AlumnoModel();
  }

  ngOnInit() {
    this._AlumnoSrv.listarAlumnosSima();
    this._AlumnoSrv.listarAlumnosSistemas();
  }
  bucarxCodigo(basicModal1) {
    this.encontrado = this._AlumnoSrv.buscarAllSistemas(this.buscaCodigo);
    console.log(this.encontrado);
    if (this.encontrado !== null) {
      if (this.encontrado.semestreInicio === null || this.encontrado.semestreInicio === undefined) {
        console.log('vacio');
        this.encontrado.semestreInicio = '';
        this.encontrado.reciboInicio = '';
        this.encontrado.fechaInicio = '';
        this.encontrado.constanciaInicio = '';
        this.editar = false;
      } else {
        this.editar = true;
      }

    } else {
      basicModal1.hide();
      swal('Mal!', 'Alumno no encontrado', 'warning');
      this.encontrado = new AlumnoModel();
    }
  }

  SaveOrUpdateEgresado(basicModal1) {
    basicModal1.hide();
    console.log(this.encontrado);
    if (this.editar === false) {
      this._AlumnoSrv.SaveAlumnoSistemas(this.encontrado);
    } else {
      this._AlumnoSrv.UpdateAlumnoSistemas(this.encontrado);
    }

  }

  createPDF() {
    var sTable = document.getElementById('imprimirpfg').innerHTML;
    var style = "<style>";
    style = style + "div.a4page {font-family: Tahoma; width: 100mm; height: 155mm; padding: 250px;}";
    style = style + "p { text-align: justify;text-justify: inter-word;font-size: 15px;}";
    style = style + "h3 {text-align: center; text-decoration: underline; margin-top: 50px;margin-bottom: 50px;font-family: Tahoma; font-weight: 700; }";
    style = style + ".fecha {margin-top: 100px;}";
    style = style + ".firma {margin-top: 100px;}";
    style = style + ".cuerpo {margin: 50px;}";
    style = style + ".cuerpo p {line-height: 200%;}";
    style = style + ".concopia {float: left;margin-top: 160px;}";
    style = style + ".concopia p {font-size: 9px;font-family: Tahoma;}";
    style = style + ".escuela {float: right;margin-top: 170px;border-left: 3px solid black;}";
    style = style + ".escuela p {margin-left: 5px;font-size: 9px;font-family: Tahoma;}";
    style = style + "img.peque {margin-left: 50px;width: 430px; height: 60px;}";
    style = style + "</style>";
    // CREATE A WINDOW OBJECT.
    var win = window.open('', '', 'height=700,width=700');
    win.document.write('<html><head>');
    win.document.write('<title>CONSTANCIA DE EGRESADO</title>');   // <title> FOR PDF HEADER.
    win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write('</body></html>');
    win.document.close(); 	// CLOSE THE CURRENT WINDOW.
    win.print();    // PRINT THE CONTENTS.
  }

}
