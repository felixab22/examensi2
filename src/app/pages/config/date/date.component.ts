import { Component, OnInit } from '@angular/core';
import { DateModel } from 'src/app/model/date.model';
declare var swal: any;
@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.scss']
})
export class DateComponent implements OnInit {
  sexos: { id: number; tipo: string; valor: boolean; }[];
  newDate: DateModel;

  constructor() {

    this.newDate = new DateModel();
  }

  ngOnInit() {

    if (localStorage.getItem('data') !== null) {
      this.newDate = JSON.parse(localStorage.getItem('data'));
    } else {
      this.newDate = new DateModel();
    }
    this.sexos = [
      { id: 1, tipo: 'Masculino', valor: true },
      { id: 2, tipo: 'Femenino', valor: false }
    ];
    this.SaveOrUpdatedData();
  }


  radioSelectSexo(sexo1) {
    console.log(sexo1);
    this.newDate.sexoDirector = sexo1.valor;

  }
  radioDesSelectSexo() {
    Array.from(document.querySelectorAll('[name=radioNameSexo]')).forEach((x: any) => x.checked = false);
    this.newDate.sexoDirector = null;
  }

  radioSelectSexoSecre(sexo2) {
    this.newDate.sexoSecretaria = sexo2.valor;

  }
  radioDesSelectSexoSecre() {
    Array.from(document.querySelectorAll('[name=radioNameSexoSecre]')).forEach((x: any) => x.checked = false);
    this.newDate.sexoSecretaria = null;
  }
  SaveOrUpdatedData() {
    this.newDate.anio = 'AÑO DE LA LUCHA CONTRA LA CORRUPCIÓN Y LA IMPUNIDAD';
    this.newDate.directorName = 'PORRAS FLORES, Efraín Elías';
    // swal('Bien!', 'Guardado!', 'success');
    localStorage.setItem('data', JSON.stringify(this.newDate));
  }
}
