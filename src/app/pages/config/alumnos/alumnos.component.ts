import { Component, OnInit } from '@angular/core';
import { AlumnosService } from 'src/app/services/services.index';
import { AlumnoModel } from 'src/app/model/alumno.model';

@Component({
  selector: 'app-alumnos',
  templateUrl: './alumnos.component.html',
  styleUrls: ['./alumnos.component.scss']
})
export class AlumnosComponent implements OnInit {
  alumnos: AlumnoModel[];
  term = '';
  p: number = 1;
  constructor(
    public _AlumnosSrv: AlumnosService
  ) { }

  ngOnInit() {
    this.alumnos = this._AlumnosSrv.listarAlumnosSima();
    console.log(this.alumnos);
  }


}
