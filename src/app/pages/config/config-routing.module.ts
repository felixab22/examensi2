import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DateComponent } from './date/date.component';
import { AlumnosComponent } from './alumnos/alumnos.component';
import { ProfesorComponent } from './profesor/profesor.component';

const routes: Routes = [
  {
    path: 'Datos',
    component: DateComponent
  },
  {
    path: 'Lista',
    component: AlumnosComponent
  },
  {
    path: 'Profesores',
    component: ProfesorComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigRoutingModule { }
