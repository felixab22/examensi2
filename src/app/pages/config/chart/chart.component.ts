import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { EstadisticaService } from '../../../services/estadistica.service';
import { DOCUMENT } from '@angular/common';


@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {
  @Output() cambioValor: EventEmitter<any> = new EventEmitter();
  menus: any[];

  constructor(
    @Inject(DOCUMENT) private _document,
    private _graficasSrv: EstadisticaService
  ) {
    this.menus = this._graficasSrv.menus;
  }

  ngOnInit() {
  }
  emitValor(item: any, link: any) {
    // console.log(item);

    const selectores: any = this._document.getElementsByClassName('selector');
    for (const ref of selectores) {
      ref.classList.remove('colorAzul');
    }
    link.classList.add('colorAzul');

    if (item.id === 1) {
      let entrada = this._graficasSrv.getgraficoIngreso();
      console.log(entrada);
      this.cambioValor.emit(entrada);

    }
    if (item.id === 2) {
      let entrada = this._graficasSrv.getgraficoEgresado();
      console.log(entrada);
      this.cambioValor.emit(entrada);
    }

  }
}
