import { Component, OnInit } from '@angular/core';
import { ProfesorService } from 'src/app/services/profesor.service';
import { ProfesorModel } from 'src/app/model/profesor.model';
declare var swal: any;

@Component({
  selector: 'app-profesor',
  templateUrl: './profesor.component.html',
  styleUrls: ['./profesor.component.scss']
})
export class ProfesorComponent implements OnInit {
  profesor: ProfesorModel;
  profesores: any;
  constructor(
    private _ProvesorSrv: ProfesorService,
  ) {
    this.profesor = new ProfesorModel();
  }

  ngOnInit() {
    this._ProvesorSrv.getAllProfesores();
    this.listarProfes();
  }
  GuardaProfesor(profesor: ProfesorModel) {
    this._ProvesorSrv.SaveProfesor(profesor);
    this.profesor = new ProfesorModel();
    this.listarProfes();
  }
  listarProfes() {
    this.profesores = this._ProvesorSrv.listarProfesores();
    // this.profesores = this._ProvesorSrv.getAllProfesores();
  }

  eliminar(key) {
    swal({
      title: "Esta seguro?",
      text: "Cuidado, Está por eliminar!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          this._ProvesorSrv.deleteProfesor(key);
          this.listarProfes();
          swal("Profesor eliminado", {
            icon: "success",
          });
        } else {
          swal("Operación cancelada!");
        }
      });
  }
}
