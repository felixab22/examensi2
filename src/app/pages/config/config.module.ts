import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfigRoutingModule } from './config-routing.module';
import { DateComponent } from './date/date.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { ServicesModule } from 'src/app/services/services.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AlumnosComponent } from './alumnos/alumnos.component';

import { NgxPaginationModule } from 'ngx-pagination';
import { ProfesorComponent } from './profesor/profesor.component';
import { ChartComponent } from './chart/chart.component'; // <-- import the module

@NgModule({
  declarations: [
    DateComponent,
    AlumnosComponent,
    ProfesorComponent,
    ChartComponent],
  imports: [
    NgxPaginationModule,
    CommonModule,
    ConfigRoutingModule,
    //desde aqui para MDBootstrap
    MDBBootstrapModule.forRoot(),
    // BrowserModule,
    // AppRoutingModule,
    ServicesModule,
    Ng2SearchPipeModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    //siempre importar
  ],
  exports: [
    DateComponent,
    ChartComponent
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class ConfigModule { }
