import { Component, OnInit } from '@angular/core';
import { DateModel } from 'src/app/model/date.model';
import { AlumnosService } from 'src/app/services/services.index';

declare var swal: any;
@Component({
  selector: 'app-bienvenido',
  templateUrl: './bienvenido.component.html',
  styleUrls: ['./bienvenido.component.scss']
})
export class BienvenidoComponent implements OnInit {
  newDate: DateModel;
  listaAlumnos: import("c:/angular8/practicasUNSCH/src/app/model/alumno.model").AlumnoModel[];
  constructor(
    private _AlumnoSrv: AlumnosService
  ) {
    this.newDate = new DateModel();
  }

  ngOnInit() {
    this.newDate.anio = 'AÑO DE LA LUCHA CONTRA LA CORRUPCIÓN Y LA IMPUNIDAD';
    this.newDate.directorName = 'PORRAS FLORES, Efraín Elías';
    // swal('Bien!', 'Guardado!', 'success');
    localStorage.setItem('data', JSON.stringify(this.newDate));
    this.listaAlumnos = this._AlumnoSrv.listarAlumnosSima();
    // localStorage.setItem(JSON.stringify(this.listaAlumnos), 'listaAlumnos');
  }


}
