import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { SharedModule } from '../shared/shared.module';

import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { PagesComponent } from './pages.component';
import { BienvenidoComponent } from './bienvenido/bienvenido.component';


import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ChartsModule, WavesModule } from 'angular-bootstrap-md';


@NgModule({
  declarations: [
    PagesComponent,
    BienvenidoComponent,


  ],
  imports: [
    MDBBootstrapModule.forRoot(),
    RouterModule,
    SharedModule,
    Ng2SearchPipeModule,
    BrowserModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    ChartsModule, WavesModule,
    // NosotrosModule,

  ],
  exports: [
    BienvenidoComponent
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class PagesModule { }
